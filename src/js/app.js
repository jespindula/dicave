'use strict';

window.$ = window.jQuery = require('jquery/dist/jquery.js');

require('slick-carousel/slick/slick.js');

//require('bootstrap-sass/assets/javascripts/bootstrap/affix.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/alert.js');
require('bootstrap-sass/assets/javascripts/bootstrap/button.js');
require('bootstrap-sass/assets/javascripts/bootstrap/carousel.js');
require('bootstrap-sass/assets/javascripts/bootstrap/collapse.js');
require('bootstrap-sass/assets/javascripts/bootstrap/dropdown.js');
require('bootstrap-sass/assets/javascripts/bootstrap/modal.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/popover.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js');
require('bootstrap-sass/assets/javascripts/bootstrap/tab.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/tooltip.js');
require('bootstrap-sass/assets/javascripts/bootstrap/transition.js')

require('cd-pretty-photo/index.js');

$(document).on('ready', function () {

    // Slick carousel init
    $('.slick').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
      },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
      },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
    });

    // Bootstrap tabs init
    $('.nav-tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    // Bootstrap accordion init
    var accordions = $('.panel-group'),
        activeClass = 'panel-active';

    $(accordions).find(' > .panel').on('show.bs.collapse', function (e) {
        accordions.find('.panel-heading').removeClass(activeClass);
        $(this).find('.panel-heading').addClass(activeClass);
    });

    // Bootstrap tabs init
    var tabs = $('.tabs .tab');
    $(tabs).on('click', function (e) {
        e.preventDefault();

        var siblingTabs = $(this).closest('.tabs').find('.tab');

        siblingTabs.removeClass('active').find('a').each(function (i, el) {
            $($(el).attr('href')).hide();
        });

        $($(this).find('a').attr('href')).show();
        $(this).addClass('active');
    });

    // Yamm init
    $('.yamm .dropdown-menu').on('click', function (e) {
        e.stopPropagation();
    });
});

$(".filter").click(function () {
    $(".form-hide-busca").slideToggle("slow", function () {});
});

$(".filter1").click(function () {
    $(".busca-borda").slideToggle("slow", function () {});
});

//    $('input[name=fone]').inputmask('(99) 9999-9999[9]'); 


$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: true,
    centerMode: true,
    focusOnSelect: true
});

$(".tab-content2").hide(); //Hide all content
$(".tab-nav2 li:first").addClass("active").show(); //Activate first tab
$(".map").show(); //Show first tab content
//	$(".tab-content2:first").show(); //Show first tab content
//    $( ".map" ).slideToggle( "slow", function() {});
$(".tab-nav2 li").click(function () {
    $(".tab-nav2 li").removeClass("active"); //Remove any "active" class
    $(this).addClass("active"); //Add "active" class to selected tab
    $(".tab-content2").hide(); //Hide all tab content
    var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
    $(activeTab).fadeIn(); //Fade in the active ID content
    $('html,body').animate({
            scrollTop: $('.tab-anchor').offset().top - 150
        }, 500);
    return false;
});

$(".tab-content3").hide(); //Hide all content
//	$(".tab-nav3 li:first").addClass("active").show(); //Activate first tab
$(".map3").show(); //Show first tab content
//	$(".tab-content2:first").show(); //Show first tab content
//    $( ".map" ).slideToggle( "slow", function() {});
$(".tab-nav3 li").click(function () {
    $(".tab-nav3 li").removeClass("active"); //Remove any "active" class
    $(this).addClass("active"); //Add "active" class to selected tab
    $(".tab-content3").hide(); //Hide all tab content
    var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
    $(activeTab).fadeIn(); //Fade in the active ID content
    return false;
});

//$(document).ready(function () {
    $("area[rel^='prettyPhoto']").prettyPhoto();

    $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({
        animation_speed: 'normal',
        theme: 'light_square',
        slideshow: 3000,
        autoplay_slideshow: true
    });
    $(".gallery a[rel^='prettyPhoto']").prettyPhoto({
        animation_speed: 'fast',
        slideshow: 10000,
        hideflash: true
    });
//});